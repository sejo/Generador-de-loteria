// Generador de lotería

// Parámetros
int N_COLUMNAS = 3; // Número de columnas en la tabla
int N_FILAS = 4; // Número de filas en la tabla
int N_TABLAS = 32; // Número de tablas a generar
int N_CARTAS = 30; // Número total de tarjetas
float MARGEN_PC = 0.05; // Porcentaje de margen respecto al ancho
boolean USA_IMAGENES = true; // Usa o no las imagenes del fólder
int N_IMAGENES = N_CARTAS; // Número de imágenes en el fólder (numeradas del 0 a N-1)
color LINES_COLOR = color(150); // Color de los bordes

// Variables
int N_CELDAS = N_COLUMNAS * N_FILAS;
int [] tabla = new int[N_CELDAS];
PImage [] imagenes = new PImage[N_IMAGENES];

// Proceso
void setup(){
	size(3300,2550);
	background(255);
	stroke(LINES_COLOR);
	textSize(height/35);

	// Carga imágenes
	if(USA_IMAGENES){
		for(int i=0; i<N_IMAGENES; i++){
			imagenes[i] = loadImage("imagenes/"+i+".png");
		}
	}

	int contadorPaginas = 1;

	for(int i=0; i<N_TABLAS; i++){
		println("Tabla "+(i+1));
		generaTabla();

		pushMatrix();
		if(i%2==1){
			translate(width/2,0);
		}
		else{
			background(255);
			line(width/2,0, width/2, height);
		}
		dibujaTabla();
		popMatrix();

		if(i%2==1){
			save(String.format("output/Pag%02d.png",contadorPaginas));
			contadorPaginas++;
		}
	}


	exit();
}



// Funciones
void dibujaTabla(){
	int col, fila;
	float x,y,w,h;
	float ANCHO_TABLA = width/2;
	float ALTO_TABLA = height;
	float margenHorizontal = ANCHO_TABLA*MARGEN_PC;
	float margenVertical = margenHorizontal;
	float textoW;
	PImage imagen;
	float imagenW;
	int numTarjeta;
	w = (ANCHO_TABLA - 2*margenHorizontal - (N_COLUMNAS-1)*margenHorizontal/2)/N_COLUMNAS;
	h = (ALTO_TABLA - 2*margenVertical - (N_FILAS-1)*margenVertical/2)/N_FILAS;

	x = margenVertical;
	y = margenVertical;
	for(int i=0; i<N_CELDAS; i++){
		col = i%N_COLUMNAS;
		fila = i/N_COLUMNAS;

		numTarjeta = tabla[i];

		if(USA_IMAGENES){
			imagen = imagenes[numTarjeta];
			imagenW = h*imagen.width/imagen.height;
			image(imagen, x + (w-imagenW)/2, y, imagenW, h);
		}
		else{
			noFill();
			rect(x,y,w,h);
			fill(LINES_COLOR);
			text(numTarjeta, x + (w-textWidth(""+tabla[i]))/2,y+h/2);
		}

		x += w + margenHorizontal/2;

		if(col == N_COLUMNAS-1){
			y += h + margenVertical/2;
			x = margenHorizontal;
		}
	}


}

void generaTabla(){
	inicializaTabla();
	int numero;
	for(int i=0; i<N_CELDAS; i++){
		do{
			numero = int( random(N_CARTAS) );
		}while( estaEnLaTabla(numero) );
		tabla[i] = numero;
		print(tabla[i]+"\t");
		if(i%N_COLUMNAS == N_COLUMNAS -1 ){
			println();
		}
	}
	println("---");
}

void inicializaTabla(){
	for(int i=0; i<N_CELDAS; i++){
		tabla[i] = -1;
	}
}

boolean estaEnLaTabla(int numero){
	for(int i=0; i<N_CELDAS; i++){
		if(numero == tabla[i]){
			return true;
		}
	}
	return false;
}
