# Generador-de-loteria
Programa para generar tablas de lotería con Processing.

Ajusta los parámetros, y al ejecutarlo se generarán imágenes PNG con dos tablas cada una, pensando en un tamaño carta.

# Generación de PDF
Para convertir las imágenes a un documento PDF, usé los siguientes pasos:

* Crear un folder PDF
* Convertir cada imagen a un documento PDF a 300dpi usando Imagemagick
```
$ for f in *.png; do convert -verbose $f -quality 100 -units PixelsPerInch -density 300x300 PDF/${f%png}pdf; done
```
* Unir los PDF con pdftk
```
pdftk *pdf cat output Paginas.pdf
```
